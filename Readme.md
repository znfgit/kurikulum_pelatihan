Nufaza Knowledge Base Meeting
=============================

## Tim Ahli

1. Abah
2. Teteh
3. Oman
4. Hahaj
5. Choi
6. Fajri
7. Khalid
8. Uje
9. Ahmad

## Filosofi

> Menyusun kurikulum, materi dan study pack knowledge base perusahaan, agar pelaksanaan
pelatihan efisien dan fokus serta dalam waktu singkat dapat mengarahkan para peserta
untuk dapat belajar mandiri dengan mudah, terarah dan segera bisa ikut berproduksi.


## Materi

1.  Standardisasi kerja coder dan staf teknis
    -  IDE / Coding Tools
        1.   Zend Studio
        2.   Sublime
            *   Basic usage: Write, copy, paste, search, replace, find files
            *   Multiple cursor
            *   Snippets
            *   Projects
            *   Documenting
            *   Plugins: SublimeCodeIntel, SublimeLinter, DocBlockr, MarkdownViewer
    -   Development Environment VM Management by Docker
        1.  Installing
        2.  Copying
        3.  Making changes
        4.  Pushing changes
    -   Generate documentation by daux.io (?) 
    -   Project & issue management
		1.  Code management with GIT
			*  Initializing
			*  Initial push
			*  Cloning
			*  Pulling
			*  Pushing
			*  Conflict resolution
			*  Branching
			*  Merging
        2.  Issue Management
		    *  Creating issues
			*  Solving issues in code and committing it
		3.  Asana.com
			*  Projects, Task
		    *  Linking Asana with Git		
			*  Mobile App
    -   Knowledge Base Management

2. Pelatihan teknis Software Engineering & praktek
    -   SDLC overview
    -   Agile Development overview
    -   Sofware Engineering with Power Designer
        1. Designing Contex Diagram
        2. Designing DFD
        3. Designing Conceptual Diagram
        4. Designing Logical Diagram
        5. Designing Physical Diagram
    -   Web Development Practice
        1.  Composer basics
        2.  Silex basics
            *  Overview
            *  Routing (Immediate)
            *  Routing (Forwarded to Class)
            *  Getting variables
            *  Templating with Twig
            *  Authentication with Symphony Security module
            *  Authorization concept with Firewalls
        3.  Propel basics 
            *  Overview
            *  Configuration files
            *  Reverse Engineer
            *  Build OM
        4.  Extjs 5
            *  Overview
            *  Sencha Command Create, Build, Refresh
            *  MVC basics (namespace, require, etc)
            *  Themes
            *  Components
            *  Layouts
            *  Events
            *  Data & REST
            *  Charting
        5.  Xond 3
            *  Overview
            *  Installation
            *  Generating Table & Column Infos
            *  Costumizing Table & Column Infos
            *  Generating Components
            *  Extending & modifying components
            *  Using components
        6.  Membuat web dengan Bolt CMS
            *  Overview
            *  Installation
            *  Plug-ins
		7. Building interactive, responsive, and beautiful single page website using bootstrap, jquery, css3, and angular JS
			* css3
				1. introduction to css3
				2. building website layout using css3
			* bootstrap
				1. deploying bootstrap css library
				2. using bootstrap to beautify website elements
				3. choosing bootstrap theme
			* jquery
				1. introduction to jquery
				2. deploying jquery code to project
				3. using jquery to build interactive website UI
			* angular JS
				1. introduction to angular js
				2. deploying angular js code to website
				3. using angular js as router to build single page website
			* integration angular js with silex
				1. integrating double routing (backend routing and front end routing) between silex and angular js


3.  Pemahaman sistem pendidikan dan pembinaan guru 
    -  Overview Mekanisme Karir Guru
    -  Overview Analisis Kecukupan guru
    -  Overview Standardisasi Pendidikan
    -  Overview Analisis Data Pendidikan

4.  Pemahaman sistem administrasi pelaporan dan keuangan
    -  Administrasi
        1. Perpres 54 2010
        2. KAK
        3. Ustek
        4. Kontrak

    -  Pelaporan
        1. Memahami sistematika proyek
        2. Membuat kerangka gambaran proyek
        3. Membaca dan memahami user interface
        4. Membaca dan memahami kode sumber
        5. Membaca dan memahami basis data
		
## Harus Dibuat

1. Silabus (lengkapi di atas)
2. Makalah
3. Study Pack




